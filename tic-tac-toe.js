// Задаем игровые символы
const xSymbol = 'x';
const oSymbol = 'o';

// Задаем различные состояния игрового поля
const gameFieldXVerticalWin = [
    [xSymbol, oSymbol, null],
    [xSymbol, null, oSymbol],
    [xSymbol, oSymbol, oSymbol],
];

const gameFieldXHorizontalWin = [
    [oSymbol, null, null],
    [xSymbol, xSymbol, xSymbol],
    [xSymbol, oSymbol, oSymbol],
];

const gameFieldXDiagonalWin = [
    [xSymbol, oSymbol, null],
    [oSymbol, xSymbol, oSymbol],
    [oSymbol, oSymbol, xSymbol],
];

const gameFieldOVerticalWin = [
    [xSymbol, oSymbol, xSymbol],
    [xSymbol, oSymbol, oSymbol],
    [null, oSymbol, xSymbol],
];

const gameFieldOHorizontalWin = [
    [xSymbol, oSymbol, null],
    [oSymbol, oSymbol, oSymbol],
    [oSymbol, xSymbol, xSymbol],
];

const gameFieldODiagonalWin = [
    [null, xSymbol, oSymbol],
    [xSymbol, oSymbol, xSymbol],
    [oSymbol, xSymbol, oSymbol],
];

const gameFieldDraw = [
    [xSymbol, oSymbol, xSymbol],
    [xSymbol, oSymbol, oSymbol],
    [oSymbol, xSymbol, xSymbol],
];

const gameFieldBegin = [
    [null, null, null],
    [null, xSymbol, null],
    [oSymbol, null, null],
];

// Задаем объявление победителя
const winner = {
    [xSymbol]: 'Крестики победили',
    [oSymbol]: 'Нолики победили'
};

// Задаем функцию обхода
function enumeration(gameField) {
    // Проверка по горизонтали
    for (let i = 0; i < gameField.length; i++) {
        if (gameField[i][0] === gameField[i][1] && gameField[i][1] === gameField[i][2] && gameField[i][0] != null) {
            return winner[gameField[i][0]]
        }
    }

    // Проверка по вертикали
    for (let i = 0; i < gameField[0].length; i++) {
        if (gameField[0][i] === gameField[1][i] && gameField[1][i] === gameField[2][i] && gameField[0][i] != null) {
            return winner[gameField[0][i]]
        }
    }

    // Проверка главной диагонали
    if (gameField[0][0] === gameField[1][1] && gameField[1][1] === gameField[2][2] && gameField[1][1] != null) {
        return winner[gameField[1][1]]
    }

    // Проверка побочной диагонали
    if (gameField[0][2] === gameField[1][1] && gameField[1][1] === gameField[2][0] && gameField[1][1] != null) {
        return winner[gameField[1][1]]
    }

    // Проверка незаконченной игры
    for (let i = 0; i < gameField.length; i++) {
        for (let j = 0; j < gameField[i].length; j++) {
            if (gameField[i][j] === null) {
                return "Игра продолжается"
            }
        }
    }
    // Объявление ничьей
    return "Ничья"
}

// Тест различных состояний игрового поля
console.log(enumeration(gameFieldXVerticalWin), '- на игровой доске есть 3 заполненные клетки с крестиками по вертикали')
console.log(enumeration(gameFieldXHorizontalWin), '- на игровой доске есть 3 заполненные клетки с крестиками по горизонтали')
console.log(enumeration(gameFieldXDiagonalWin), '- на игровой доске есть 3 заполненные клетки с крестиками по диагонали')
console.log(enumeration(gameFieldOVerticalWin), '- на игровой доске есть 3 заполненные клетки с ноликами по вертикали')
console.log(enumeration(gameFieldOHorizontalWin), '- на игровой доске есть 3 заполненные клетки с ноликами по горизонтали')
console.log(enumeration(gameFieldODiagonalWin), '- на игровой доске есть 3 заполненные клетки с ноликами по диагонали')
console.log(enumeration(gameFieldDraw), '- на игровой доске нет 3 заполненных клеток с крестиками или ноликами')
console.log(enumeration(gameFieldBegin))

